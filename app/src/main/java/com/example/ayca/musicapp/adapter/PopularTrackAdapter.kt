package com.example.ayca.musicapp.adapter

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.musicapp.Model.Track
import com.example.ayca.musicapp.Model.TrackList
import com.example.ayca.musicapp.R
import com.google.gson.Gson

class PopularTrackAdapter(
    private var trackList: List<TrackList>,
    private var context: Context,
    private var isVisible: Boolean
) : RecyclerView.Adapter<PopularTrackAdapter.ViewHolder>() {

    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView),
        View.OnClickListener {
        var trackNameText: TextView
        var trackRatingText: TextView
        var artistNameText: TextView
        var favBtn: ImageButton
        var isClick: Boolean = false
        var favTrackList: MutableList<Track> = mutableListOf()
        var preferences: SharedPreferences
        var editor: SharedPreferences.Editor

        init {
            trackNameText = itemLayoutView.findViewById(R.id.track_name_txt)
            trackRatingText = itemLayoutView.findViewById(R.id.track_rating_txt)
            artistNameText = itemLayoutView.findViewById(R.id.artist_name_txt)
            favBtn = itemLayoutView.findViewById(R.id.fav_btn)
            favBtn.setOnClickListener(this)



            preferences =
                itemLayoutView.context.getSharedPreferences("favTrackList", Context.MODE_PRIVATE)
            editor = preferences.edit()

        }

        override fun onClick(v: View?) {
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.popular_song_list_design,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return trackList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tracks = trackList.get(position).track
        holder.trackNameText.text = tracks.track_name
        if (isVisible) {
            holder.trackRatingText.text = tracks.track_rating.toString()
            holder.artistNameText.text = tracks.artist_name


        } else {
            holder.trackRatingText.visibility = View.GONE
            holder.artistNameText.visibility = View.GONE
            holder.trackNameText.setTypeface(null, Typeface.NORMAL)
        }

        if (holder.preferences.getString(
                "favTrackList",
                null
            )?.contains(tracks.track_name)!!

        ) {
            holder.favBtn.setImageResource(R.drawable.fav_click_small_ic)
            holder.isClick = true
        } else {
            holder.favBtn.setImageResource(R.drawable.fav_small_ic)
            holder.isClick = false
        }

        holder.itemView.setOnClickListener {
            clickSong(position, it)
        }

        holder.favBtn.setOnClickListener {
            if (holder.isClick) {

                holder.favBtn.setImageResource(R.drawable.fav_small_ic)
                holder.isClick = false


                var trackList  =Gson().fromJson(holder.preferences.getString("favTrackList",""),Array<Track> ::class.java).toList()

                holder.favTrackList.addAll(trackList)
                holder.favTrackList.remove(tracks)

                var favTrackListString = Gson().toJson(holder.favTrackList)

                holder.editor.putString("favTrackList", favTrackListString)
                holder.editor.apply()


            } else {
                holder.favBtn.setImageResource(R.drawable.fav_click_small_ic)
                holder.isClick = true


                var trackList  =Gson().fromJson(holder.preferences.getString("favTrackList",""),Array<Track> ::class.java).toList()

                holder.favTrackList.addAll(trackList)
                holder.favTrackList.add(tracks)
                var favTrackListString = Gson().toJson(holder.favTrackList)


                holder.editor.putString("favTrackList", favTrackListString)
                holder.editor.apply()


            }
        }
    }

    private fun clickSong( position: Int, view: View) {
        val bundle = Bundle()
        bundle.putInt("trackId", trackList.get(position).track.track_id)

        Navigation.findNavController(view).navigate(R.id.trackDetailFragment,bundle)
    }
}
