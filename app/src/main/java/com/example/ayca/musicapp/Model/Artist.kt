package com.example.ayca.musicapp.Model

import com.google.gson.annotations.SerializedName

data class Artist(

    @SerializedName("artist_id")
    val artist_id: Int,

    @SerializedName("artist_name")
    val artist_name: String,

    @SerializedName("artist_country")
    val artist_country: String,

    @SerializedName("artist_rating")
    val artist_rating: Int,

    @SerializedName("artist_twitter_url")
    val artist_twitter_url: String
)