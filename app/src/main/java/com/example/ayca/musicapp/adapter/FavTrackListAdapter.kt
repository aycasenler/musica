package com.example.ayca.musicapp.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.musicapp.Model.Track
import com.example.ayca.musicapp.R

class FavTrackListAdapter (
    private var trackList : List<Track>,
    private var context: Context
) : RecyclerView.Adapter<FavTrackListAdapter.ViewHolder>() {

    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView){
        var trackNameText: TextView
        var artistNameTxt : TextView


        init {
            trackNameText = itemLayoutView.findViewById(R.id.track_name_txt)
            artistNameTxt = itemLayoutView.findViewById(R.id.artist_name_txt)

        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.fav_track_line_design,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return trackList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.trackNameText.text = trackList[position].track_name
        holder.artistNameTxt.text = trackList[position].artist_name

        holder.itemView.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("trackId",  trackList[position].track_id)
            Navigation.findNavController(it).navigate(R.id.trackDetailFragment,bundle)
        }


    }



}
