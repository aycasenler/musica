package com.example.ayca.musicapp.Model

import com.google.gson.annotations.SerializedName

data class Album(
    @SerializedName("album_id")
    val album_id : Int,
    @SerializedName("album_name")
    val album_name : String,
    @SerializedName("album_rating")
    val album_rating : Int,
    @SerializedName("album_release_date")
    val album_release_date : String,
    @SerializedName("artist_id")
    val artist_id : Int,
    @SerializedName("artist_name")
    val artist_name : String,
    @SerializedName("primary_genres")
    val primary_genres : MusicGenreList,
    @SerializedName("album_pline")
    val album_pline : String,
    @SerializedName("album_copyright")
    val album_copyright : String,
    @SerializedName("album_label")
    val album_label : String

)