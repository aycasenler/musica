package com.example.ayca.musicapp.Model

import com.google.gson.annotations.SerializedName

data class Track(
    @SerializedName("track_id")
    val track_id : Int,
    @SerializedName("track_name")
    val track_name : String,
    @SerializedName("track_rating")
    val track_rating : Int,
    @SerializedName("album_name")
    val album_name : String,
    @SerializedName("album_id")
    val album_id : Int,
    @SerializedName("artist_name")
    val artist_name : String,
    @SerializedName("track_share_url")
    val track_share_url : String,
    @SerializedName("primary_genres")
    val primary_genres : MusicGenreList
)