package com.example.ayca.musicapp
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.musicapp.Model.Artist

class FavArtistListAdapter(
    private var artistList : List<Artist>,
    private var context: Context
) : RecyclerView.Adapter<FavArtistListAdapter.ViewHolder>() {

    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView){
        var artistNameText: TextView


        init {
            artistNameText = itemLayoutView.findViewById(R.id.artist_name_txt)

        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.fav_artist_line_design,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return artistList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

       holder.artistNameText.text = artistList[position].artist_name
        holder.itemView.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("artistName",  artistList[position].artist_name)
            bundle.putString("artistCountry",  artistList[position].artist_country)
            bundle.putString("artistTwitterUrl",  artistList[position].artist_twitter_url)
            bundle.putInt("artistId",  artistList[position].artist_id)
            Navigation.findNavController(it).navigate(R.id.detailArtistFragment,bundle)
        }

    }



}
