package com.example.ayca.musicapp

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.musicapp.Model.*
import com.example.ayca.musicapp.adapter.MyArtistListAdapter
import com.example.ayca.musicapp.adapter.PopularArtistAdapter
import com.example.ayca.musicapp.adapter.PopularTrackAdapter
import com.example.ayca.musicapp.retrofit.ApiClient
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.ThreeBounce
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeScreenFragment : Fragment(), View.OnClickListener {
    lateinit var recyclerViewArtist: RecyclerView
    lateinit var recyclerViewMyArtist : RecyclerView
    lateinit var artistList: List<ArtistList>
    lateinit var trackList: List<TrackList>
    lateinit var artistAdapter: PopularArtistAdapter
    lateinit var trackAdapter: PopularTrackAdapter
    lateinit var myArtistListAdapter : MyArtistListAdapter
    lateinit var recyclerViewTrack: RecyclerView
    lateinit var searchBtn: ImageButton
    lateinit var favBtn: ImageButton
    lateinit var artistPreferences: SharedPreferences
    lateinit var trackPreferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    var trackEmptyList: List<Track> = mutableListOf()
    var artistEmptyList: List<Artist> = mutableListOf()
    lateinit var progressBar: ProgressBar
    lateinit var blurLayout : LinearLayout
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.home_screen_fragment, container, false)
        initView(view)

        return view
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("CommitPrefEdits")
    private fun initView(view: View) {
        recyclerViewArtist = view.findViewById(R.id.recycler_view_artist)
        recyclerViewArtist.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        artistList = mutableListOf()
        recyclerViewTrack = view.findViewById(R.id.recycler_view_track)
        recyclerViewTrack.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        trackList = mutableListOf()
        artistPreferences =
            view.context.getSharedPreferences("favArtistList", Context.MODE_PRIVATE)
        trackPreferences =
            view.context.getSharedPreferences("favTrackList", Context.MODE_PRIVATE)

        recyclerViewMyArtist = view.findViewById(R.id.recycler_view_my_artist)
        recyclerViewMyArtist.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        if (artistPreferences.getString("favArtistList", null) == null) {

            editor = artistPreferences.edit()
            editor.putString("favArtistList", Gson().toJson(artistEmptyList))
            editor.apply()
        }
        if (trackPreferences.getString("favTrackList", null) == null) {

            editor = trackPreferences.edit()

            editor.putString("favTrackList", Gson().toJson(trackEmptyList))
            editor.apply()
        }

        progressBar = view.findViewById(R.id.progress)
        blurLayout = view.findViewById(R.id.blur_layout)

        val threeBounce : Sprite = ThreeBounce()
        progressBar.setIndeterminateDrawableTiled(threeBounce)


        getMyArtistList()
        getArtistChart()
        getTrackChart()

        searchBtn = view.findViewById(R.id.toolbar_search_btn)
        favBtn = view.findViewById(R.id.toolbar_fav_btn)

        searchBtn.setOnClickListener(this)
        favBtn.setOnClickListener(this)
    }

    private fun getMyArtistList() {
        val britneySpears = MyArtist(33491420,"Britney Spears", R.drawable.britney_spears )
        val adele = MyArtist(346898, "Adele", R.drawable.adele)
        val ladyGaga = MyArtist(378462,"Lady Gaga", R.drawable.lady_gaga)
        val shakira = MyArtist(33091389,"Shakira", R.drawable.shakira)
        val justinTimberlake = MyArtist(22681,"Justin Timberlake", R.drawable.justin_timberlake)
        val beyonce = MyArtist(18927,"Beyonce", R.drawable.beyonce)
        val rihanna = MyArtist(33491890,"Rihanna", R.drawable.rihanna)
        val arianaGrande = MyArtist(13958599,"Ariana Grande", R.drawable.ariana_grande)
        val sia = MyArtist(41659,"Sia", R.drawable.sia)
        val lilWayne = MyArtist(23619,"lilWayne", R.drawable.lil_wayne)
        val drake = MyArtist(33491453,"Drake", R.drawable.drake)
        val snoppDogg = MyArtist(38872727,"Snopp Dogg", R.drawable.snoop_dogg)


        val myArtistList = listOf(britneySpears,adele,ladyGaga,shakira,justinTimberlake,beyonce,rihanna,arianaGrande,
            sia,lilWayne,drake,snoppDogg)

        myArtistListAdapter =
            MyArtistListAdapter(myArtistList,context!!)
        recyclerViewMyArtist.adapter = myArtistListAdapter
    }

    fun getArtistChart() {
        val call: Call<MainData> = ApiClient.getClient.getArtistChart()
        call.enqueue(object : Callback<MainData> {
            override fun onFailure(call: Call<MainData>?, t: Throwable?) {
                Toast.makeText(context, "onFail!", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<MainData>?, response: Response<MainData>?) =
                if (response!!.isSuccessful) {


                    artistAdapter =
                        PopularArtistAdapter(response.body().message.body.artist_list, context!!)

                    recyclerViewArtist.adapter = artistAdapter


                } else {
                    Toast.makeText(context, "Response not successful", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    fun getTrackChart() {
        val call: Call<MainData> = ApiClient.getClient.getTrackChart()
        call.enqueue(object : Callback<MainData> {
            override fun onFailure(call: Call<MainData>?, t: Throwable?) {
                Toast.makeText(context, "onFail!", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<MainData>?, response: Response<MainData>?) =
                if (response!!.isSuccessful) {

                    blurLayout.visibility = View.GONE
                    trackAdapter =
                        PopularTrackAdapter(response.body().message.body.track_list, context!!, true)

                    recyclerViewTrack.adapter = trackAdapter


                } else {
                    Toast.makeText(context, "Response not successful", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }


    override fun onClick(v: View?) {

        when (v?.id) {
            searchBtn.id -> {
                Navigation.findNavController(v)
                    .navigate(R.id.action_homeScreenFragment_to_searchFragment)
            }

            favBtn.id -> {
                Navigation.findNavController(v)
                    .navigate(R.id.action_homeScreenFragment_to_favListFragment)
            }
        }
    }

    data class MyArtist(
        val artistId : Int,
        val artistName : String,
        val img : Int
    )



}

