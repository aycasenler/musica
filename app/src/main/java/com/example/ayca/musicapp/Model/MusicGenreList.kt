package com.example.ayca.musicapp.Model

import com.google.gson.annotations.SerializedName

data class MusicGenreList(
    @SerializedName("music_genre_list")
    val music_genre_list : List<MusicGenre>
)