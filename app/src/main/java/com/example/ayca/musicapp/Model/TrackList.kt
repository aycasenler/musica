package com.example.ayca.musicapp.Model

import com.google.gson.annotations.SerializedName

data class TrackList(
    @SerializedName("track")
    val track : Track
)