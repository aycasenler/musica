package com.example.ayca.musicapp

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.musicapp.Model.MainData
import com.example.ayca.musicapp.adapter.ArtistDataAdapter
import com.example.ayca.musicapp.adapter.TrackDataAdapter
import com.example.ayca.musicapp.retrofit.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchFragment : Fragment(), View.OnClickListener, TextWatcher {
    lateinit var recyclerView: RecyclerView
    lateinit var artistAdapter: ArtistDataAdapter
    lateinit var trackAdapter : TrackDataAdapter
    lateinit var searchEt : EditText
    lateinit var searchImgBtn : ImageButton
    lateinit var searchArtistBtn : Button
    lateinit var searchSongBtn : Button
    lateinit var homeBtn: ImageButton
    lateinit var favBtn: ImageButton
    var searchBoolean : Boolean = false
    lateinit var artistSelectedSeperator : View
    lateinit var songSelectedSeperator : View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.search_fragment, container, false )
        initView(view)
        return view
    }

    private fun initView(view: View) {
        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        searchEt = view.findViewById(R.id.search_et)
        searchImgBtn = view.findViewById(R.id.search_btn)
        searchArtistBtn = view.findViewById(R.id.search_by_artist_btn)
        searchSongBtn = view.findViewById(R.id.search_by_song_btn)

        artistSelectedSeperator = view.findViewById(R.id.artist_selected_seperator)
        songSelectedSeperator = view.findViewById(R.id.song_selected_seperator)

        homeBtn = view.findViewById(R.id.home_screen_btn)
        favBtn = view.findViewById(R.id.toolbar_fav_btn)

        searchImgBtn.setOnClickListener(this)
        searchArtistBtn.setOnClickListener(this)
        searchSongBtn.setOnClickListener(this)
        searchEt.addTextChangedListener(this)
        homeBtn = view.findViewById(R.id.home_screen_btn)
        favBtn = view.findViewById(R.id.toolbar_fav_btn)
    }
    fun searchArtist(){
        val artistName = searchEt.text.toString().trim()
        val call: Call<MainData> = ApiClient.getClient.artistSearch(artistName)
        call.enqueue(object : Callback<MainData> {
            override fun onFailure(call: Call<MainData>?, t: Throwable?) {
                Toast.makeText(context, "onFail!", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<MainData>?, response: Response<MainData>?) =
                if (response!!.isSuccessful) {

                    artistAdapter =
                        ArtistDataAdapter(response.body().message.body.artist_list, context!!)

                    recyclerView.adapter = artistAdapter
                } else {
                    Toast.makeText(context, "Response not successful", Toast.LENGTH_SHORT).show()
                }
        }
        )

    }
    fun searchTrack(){
        val trackName = searchEt.text.toString().trim()
        val call: Call<MainData> = ApiClient.getClient.trackSerch(trackName)
        call.enqueue(object : Callback<MainData> {
            override fun onFailure(call: Call<MainData>?, t: Throwable?) {
                Toast.makeText(context, "onFail!", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<MainData>?, response: Response<MainData>?) =
                if (response!!.isSuccessful) {

                    trackAdapter =
                        TrackDataAdapter(response.body().message.body.track_list, context!!, true)

                    recyclerView.adapter = trackAdapter
                } else {
                    Toast.makeText(context, "Response not successful", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    override fun onClick(v: View?) {
        when(v?.id ){

            searchArtistBtn.id ->{
                searchBoolean = false
                if(!searchEt.text.isEmpty()){
                    searchArtist()
                }
                artistSelectedSeperator.visibility = View.VISIBLE
                songSelectedSeperator.visibility = View.GONE
            }
            searchSongBtn.id ->{
                searchBoolean = true
                if(!searchEt.text.isEmpty()){
                    searchTrack()
                }
                artistSelectedSeperator.visibility = View.GONE
                songSelectedSeperator.visibility = View.VISIBLE
            }
            homeBtn.id -> {
                Navigation.findNavController(v).navigate(R.id.action_searchFragment_to_homeScreenFragment)
            }
            favBtn.id -> {
                Navigation.findNavController(v).navigate(R.id.action_searchFragment_to_favListFragment)
            }

        }
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if(searchBoolean){
            searchTrack()
        }else{
            searchArtist()
        }
    }
}