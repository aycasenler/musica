package com.example.ayca.musicapp

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.musicapp.Model.Artist
import com.example.ayca.musicapp.Model.Track
import com.example.ayca.musicapp.adapter.FavTrackListAdapter
import com.google.gson.Gson


class FavListFragment : Fragment(), View.OnClickListener {
    lateinit var preferences: SharedPreferences
    lateinit var favArtistListAdapter: FavArtistListAdapter
    lateinit var recyclerViewArtist: RecyclerView
    lateinit var favTrackListAdapter: FavTrackListAdapter
    lateinit var artistSelectedSeperator: View
    lateinit var songSelectedSeperator: View
    lateinit var artistSelectedBtn: Button
    lateinit var searchBtn: ImageButton
    lateinit var homeBtn: ImageButton
    lateinit var songSelectedBtn: Button
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fav_list_fragment, container, false)

        initView(view)
        getFavArtist()
        return view
    }

    private fun initView(view: View) {
        artistSelectedSeperator = view.findViewById(R.id.artist_selected_seperator)
        songSelectedSeperator = view.findViewById(R.id.song_selected_seperator)
        artistSelectedBtn = view.findViewById(R.id.search_by_artist_btn)
        songSelectedBtn = view.findViewById(R.id.search_by_song_btn)
        searchBtn = view.findViewById(R.id.toolbar_search_btn)
        homeBtn = view.findViewById(R.id.home_screen_btn)

        searchBtn = view.findViewById(R.id.toolbar_search_btn)
        homeBtn = view.findViewById(R.id.home_screen_btn)

        artistSelectedBtn.setOnClickListener(this)
        songSelectedBtn.setOnClickListener(this)
        searchBtn.setOnClickListener(this)
        homeBtn.setOnClickListener(this)

        recyclerViewArtist = view.findViewById(R.id.recycler_view)
        recyclerViewArtist.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerViewArtist.addItemDecoration(
            DividerItemDecoration(
                context,
                LinearLayoutManager.VERTICAL
            )
        )
    }

    private fun getFavSongs() {
        preferences = context!!.getSharedPreferences("favTrackList", Context.MODE_PRIVATE)
        favTrackListAdapter = FavTrackListAdapter(
            Gson().fromJson(
                preferences.getString("favTrackList", ""),
                Array<Track>::class.java
            ).toList()
            , context!!
        )
        recyclerViewArtist.adapter = favTrackListAdapter

        artistSelectedSeperator.visibility = View.GONE
        songSelectedSeperator.visibility = View.VISIBLE
    }

    private fun getFavArtist() {
        preferences = context!!.getSharedPreferences("favArtistList", Context.MODE_PRIVATE)

        favArtistListAdapter = FavArtistListAdapter(
            Gson().fromJson(
                preferences.getString("favArtistList", ""),
                Array<Artist>::class.java
            ).toList()
            , context!!
        )
        recyclerViewArtist.adapter = favArtistListAdapter

        artistSelectedSeperator.visibility = View.VISIBLE
        songSelectedSeperator.visibility = View.GONE
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            artistSelectedBtn.id -> {
                getFavArtist()
            }
            songSelectedBtn.id -> {
                getFavSongs()
            }
            searchBtn.id -> {
                Navigation.findNavController(v)
                    .navigate(R.id.action_favListFragment_to_searchFragment)
            }
            homeBtn.id -> {
                Navigation.findNavController(v)
                    .navigate(R.id.action_favListFragment_to_homeScreenFragment)
            }
        }
    }

}