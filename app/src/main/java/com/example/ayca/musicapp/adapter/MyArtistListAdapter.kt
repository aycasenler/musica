package com.example.ayca.musicapp.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.musicapp.HomeScreenFragment
import com.example.ayca.musicapp.R
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation

class MyArtistListAdapter(
    private var myArtistList: List<HomeScreenFragment.MyArtist>,
    private var context: Context
) : RecyclerView.Adapter<MyArtistListAdapter.ViewHolder>() {

    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var artistNameTxt: TextView
        var artistImg: ImageView




        init {
            artistNameTxt = itemLayoutView.findViewById(R.id.artist_name_txt)
            artistImg = itemLayoutView.findViewById(R.id.artist_img)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.my_artist_list_design,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return myArtistList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val artist = myArtistList.get(position)
      holder.artistNameTxt.text = artist.artistName
        Picasso.get().load(artist.img).resize(250, 250).transform(CropCircleTransformation())
            .into(holder.artistImg)

        holder.itemView.setOnClickListener {
            clickAlbum(position, it)
        }
    }



    private fun clickAlbum(position: Int, view: View) {

        val bundle = Bundle()
        bundle.putInt("artistId", myArtistList[position].artistId)
        Navigation.findNavController(view).navigate(R.id.detailArtistFragment,bundle)

    }


}
