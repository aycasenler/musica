package com.example.ayca.musicapp.adapter

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.musicapp.Model.Artist
import com.example.ayca.musicapp.Model.ArtistList
import com.example.ayca.musicapp.R
import com.google.gson.Gson

class ArtistDataAdapter(
    private var artistList: List<ArtistList>,
    private var context: Context
) : RecyclerView.Adapter<ArtistDataAdapter.ViewHolder>() {
    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var artistNameText: TextView
        //  var artistTwitterUrlText : TextView
        var artistRatingText: TextView
        var favBtn: ImageButton
        var isClick: Boolean = false
        var artistCountryTxt: TextView
        var favArtistList: MutableList<Artist> = mutableListOf()
        var preferences: SharedPreferences
        var editor: SharedPreferences.Editor
        val set = HashSet<String>()


        init {
            artistNameText = itemLayoutView.findViewById(R.id.artist_name_txt)
            artistRatingText = itemLayoutView.findViewById(R.id.artist_rating_txt)
            artistCountryTxt = itemLayoutView.findViewById(R.id.artist_country_txt)
            favBtn = itemLayoutView.findViewById(R.id.fav_btn)
            favArtistList = mutableListOf()


            preferences =
                itemLayoutView.context.getSharedPreferences("favArtistList", Context.MODE_PRIVATE)
            editor = preferences.edit()

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.artist_data_list_design,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return artistList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val artists = artistList.get(position).artist
        holder.artistNameText.text = artists.artist_name
        holder.artistRatingText.text = artists.artist_rating.toString()
        holder.artistCountryTxt.text = artists.artist_country


        if (holder.preferences.getString(
                "favArtistList",
                null
            )?.contains(artists.artist_name)!!
        ) {
            holder.favBtn.setImageResource(R.drawable.fav_click_small_ic)
            holder.isClick = true
        } else {
            holder.favBtn.setImageResource(R.drawable.fav_small_ic)
            holder.isClick = false
        }


        holder.itemView.setOnClickListener {
            clickArtist(position, it)
        }

        holder.favBtn.setOnClickListener {
            if (holder.isClick) {

                holder.favBtn.setImageResource(R.drawable.fav_small_ic)
                holder.isClick = false


                var artistList =
                    Gson().fromJson(
                        holder.preferences.getString("favArtistList", ""),
                        Array<Artist>::class.java
                    ).toList()

                holder.favArtistList.addAll(artistList)
                holder.favArtistList.remove(artists)

                var favArtistListString = Gson().toJson(holder.favArtistList)

                holder.editor.putString("favArtistList", favArtistListString)
                holder.editor.apply()

            } else {
                holder.favBtn.setImageResource(R.drawable.fav_click_small_ic)
                holder.isClick = true


                var artistList = Gson().fromJson(
                    holder.preferences.getString("favArtistList", ""),
                    Array<Artist>::class.java
                ).toList()

                holder.favArtistList.addAll(artistList)
                holder.favArtistList.add(artists)
                var favArtistListString = Gson().toJson(holder.favArtistList)


                holder.editor.putString("favArtistList", favArtistListString)
                holder.editor.apply()


            }

        }
    }


    private fun clickArtist(position: Int, view: View) {

        val bundle = Bundle()
        bundle.putString("artistName", artistList.get(position).artist.artist_name)
        bundle.putInt("artistId", artistList.get(position).artist.artist_id)
        bundle.putString("artistTwitterUrl", artistList[position].artist.artist_twitter_url)
        bundle.putString("artistCountry", artistList[position].artist.artist_country)
        bundle.putString("artistRating", artistList[position].artist.artist_rating.toString())
        bundle.putString("artists", artistList[position].artist.toString())
      //  bundle.put("artists",artistList[position].artist)
        Navigation.findNavController(view).navigate(R.id.detailArtistFragment, bundle)


    }


}
