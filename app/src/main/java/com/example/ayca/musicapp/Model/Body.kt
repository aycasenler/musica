package com.example.ayca.musicapp.Model

import com.google.gson.annotations.SerializedName

data class Body(

    @SerializedName("artist_list")
    val artist_list : List<ArtistList>,
    @SerializedName("track_list")
    val track_list : List<TrackList>,
    @SerializedName("album_list")
    val album_list : List<AlbumList>,
    @SerializedName("track")
    val track : Track,
    @SerializedName("artist")
    val artist : Artist,
    @SerializedName("lyrics")
    val lyrics : Lyrics
)