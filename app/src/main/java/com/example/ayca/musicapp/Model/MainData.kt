package com.example.ayca.musicapp.Model

import com.google.gson.annotations.SerializedName

data class MainData(

    @SerializedName("message")
    val message: Message
)