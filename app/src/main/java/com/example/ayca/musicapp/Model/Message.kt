package com.example.ayca.musicapp.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Message(

    @SerializedName("body")
    val body : Body
)