package com.example.ayca.musicapp.retrofit

import com.example.ayca.musicapp.Model.MainData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface{
    @GET("chart.artists.get?format=json&callback=callback&page_size=10&country=us&apikey=b085c86b5c76f20115268e7c14b1f1d7")
    fun getArtistChart():Call<MainData>
    @GET("chart.tracks.get?format=json&callback=callback&apikey=b085c86b5c76f20115268e7c14b1f1d7")
    fun getTrackChart():Call<MainData>
    @GET("artist.search?format=json&callback=callback&apikey=b085c86b5c76f20115268e7c14b1f1d7")
    fun artistSearch(@Query("q_artist") artistName: String) : Call<MainData>
    @GET("artist.albums.get?format=json&callback=callback&apikey=b085c86b5c76f20115268e7c14b1f1d7")
    fun getArtistAlbums(@Query("artist_id") artistId : Int) : Call<MainData>
    @GET("track.search?format=json&callback=callback&quorum_factor=1&apikey=b085c86b5c76f20115268e7c14b1f1d7")
    fun trackSerch(@Query("q_track" ) songName : String) : Call<MainData>
    @GET("album.tracks.get?format=json&callback=callback&apikey=b085c86b5c76f20115268e7c14b1f1d7")
    fun getTrack(@Query("album_id") albumId : Int) : Call<MainData>
    @GET("track.get?format=json&callback=callback&apikey=b085c86b5c76f20115268e7c14b1f1d7")
    fun getTrackDetail(@Query("track_id") trackId : Int) : Call<MainData>
    @GET("track.lyrics.get?format=json&callback=callback&apikey=b085c86b5c76f20115268e7c14b1f1d7")
    fun getLyrics(@Query("track_id") trackId: Int) : Call<MainData>
    @GET("artist.get?format=json&callback=callback&apikey=b085c86b5c76f20115268e7c14b1f1d7")
    fun getArtist(@Query("artist_id") artistId: Int ): Call<MainData>
}