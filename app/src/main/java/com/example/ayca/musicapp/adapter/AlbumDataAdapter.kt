package com.example.ayca.musicapp.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.musicapp.Model.AlbumList
import com.example.ayca.musicapp.Model.MainData
import com.example.ayca.musicapp.R
import com.example.ayca.musicapp.retrofit.ApiClient
import net.cachapa.expandablelayout.ExpandableLayout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AlbumDataAdapter(
    private var albumList: List<AlbumList>,

    private var context: Context
) : RecyclerView.Adapter<AlbumDataAdapter.ViewHolder>() {

    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var albumNameText: TextView
        var albumReleaseDateText: TextView
        var songsExpandable: ExpandableLayout
        var recyclerView: RecyclerView


        init {
            albumNameText = itemLayoutView.findViewById(R.id.album_name_tv)
            albumReleaseDateText = itemLayoutView.findViewById(R.id.album_release_date_tv)
            songsExpandable = itemLayoutView.findViewById(R.id.songs_expandable_view)
            recyclerView = itemLayoutView.findViewById(R.id.songs_recycler_view)

            recyclerView.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.album_data_list_design,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return albumList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val album = albumList.get(position).album
        holder.albumNameText.text = album.album_name
        if(album.album_release_date.isEmpty()){
            holder.albumReleaseDateText.visibility = View.GONE
        }else{
            holder.albumReleaseDateText.text = album.album_release_date
        }

        holder.itemView.setOnClickListener {
            clickAlbum(position, holder)
        }
    }

    fun getSongs(recyclerView: RecyclerView, albumId: Int) {

        val call: Call<MainData> = ApiClient.getClient.getTrack(albumId)
        call.enqueue(object : Callback<MainData> {
            override fun onFailure(call: Call<MainData>?, t: Throwable?) {
                Toast.makeText(context, "onFail!", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<MainData>?, response: Response<MainData>?) =
                if (response!!.isSuccessful) {

                    var trackAdapter =
                        TrackDataAdapter(response.body().message.body.track_list, context!!, false)

                    recyclerView.adapter = trackAdapter
                } else {
                    Toast.makeText(context, "Response not successful", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    private fun clickAlbum(position: Int, holder: ViewHolder) {
        getSongs(holder.recyclerView, albumList[position].album.album_id)
        val bundle = Bundle()
        bundle.putInt("albumId", albumList.get(position).album.album_id)

        if (holder.songsExpandable.isExpanded) {
            holder.songsExpandable.collapse()
        } else {
            holder.songsExpandable.expand()
        }

    }


}
