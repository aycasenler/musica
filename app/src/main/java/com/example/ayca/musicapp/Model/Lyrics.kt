package com.example.ayca.musicapp.Model

import com.google.gson.annotations.SerializedName

data class Lyrics(
    @SerializedName("lyrics_id")
    val lyrics_id : Int,
    @SerializedName("lyrics_body")
    val lyrics_body : String,
    @SerializedName("script_tracking_url")
    val script_tracking_url : String,
    @SerializedName("pixel_tracking_url")
    val pixel_tracking_url : String,
    @SerializedName("lyrics_copyright")
    val lyrics_copyright : String

)