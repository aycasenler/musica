package com.example.ayca.musicapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.ayca.musicapp.Model.Artist
import com.example.ayca.musicapp.Model.MainData
import com.example.ayca.musicapp.Model.MusicGenre
import com.example.ayca.musicapp.Model.Track
import com.example.ayca.musicapp.retrofit.ApiClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TrackDetailFragment : Fragment(), View.OnClickListener {

    lateinit var lyricsTxt: TextView
    lateinit var trackNameTxt: TextView
    lateinit var artistNameTxt: TextView
    lateinit var moreBtn: ImageButton
    lateinit var copyrightTxt: TextView
    lateinit var lyricUrl: String
    lateinit var searchBtn: ImageButton
    lateinit var homeBtn: ImageButton
    lateinit var favBtn: ImageButton
    lateinit var toolbarFavBtn : ImageButton
    lateinit var preferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    var favTrackList: MutableList<Track> = mutableListOf()
    var isClick: Boolean = false
    var tracks: Track? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.track_detail_fragment, container, false)

        lyricsTxt = view.findViewById(R.id.lyrics_txt)
        trackNameTxt = view.findViewById(R.id.track_name_txt)
        artistNameTxt = view.findViewById(R.id.artist_name_txt)
        moreBtn = view.findViewById(R.id.more_btn)
        copyrightTxt = view.findViewById(R.id.lyrics_copyright_txt)
        searchBtn = view.findViewById(R.id.toolbar_search_btn)
        homeBtn = view.findViewById(R.id.home_screen_btn)
        toolbarFavBtn = view.findViewById(R.id.toolbar_fav_btn)
        favBtn = view.findViewById(R.id.fav_btn)


        preferences =
            context!!.getSharedPreferences("favTrackList", Context.MODE_PRIVATE)
        editor = preferences.edit()

        moreBtn.setOnClickListener(this)
        artistNameTxt.setOnClickListener(this)
        searchBtn.setOnClickListener(this)
        homeBtn.setOnClickListener(this)
        toolbarFavBtn.setOnClickListener(this)
        getSong()
        getLyrics()

        return view
    }

    private fun getFav() {
        if (preferences.getString(
                "favTrackList",
                null
            )?.contains(tracks!!.track_name)!!

        ) {
            favBtn.setImageResource(R.drawable.fav_click_small_ic)
            isClick = true
        } else {
            favBtn.setImageResource(R.drawable.fav_small_ic)
            isClick = false
        }


        favBtn.setOnClickListener {
            if (isClick) {

                favBtn.setImageResource(R.drawable.fav_small_ic)
                isClick = false


                var trackList =
                    Gson().fromJson(
                        preferences.getString("favTrackList", ""),
                        Array<Track>::class.java
                    ).toList()

                favTrackList.addAll(trackList)
                favTrackList.remove(tracks)

                var favTrackListString = Gson().toJson(favTrackList)

                editor.putString("favTrackList", favTrackListString)
                editor.apply()


            } else {
                favBtn.setImageResource(R.drawable.fav_click_small_ic)
                isClick = true


                var trackList =
                    Gson().fromJson(
                        preferences.getString("favTrackList", ""),
                        Array<Track>::class.java
                    ).toList()

                favTrackList.addAll(trackList)
                favTrackList.add(tracks!!)
                var favTrackListString = Gson().toJson(favTrackList)


                editor.putString("favTrackList", favTrackListString)
                editor.apply()


            }
        }
    }


    fun getSong() {
        val trackId = arguments?.getInt("trackId")
        val call: Call<MainData> = ApiClient.getClient.getTrackDetail(trackId!!)
        call.enqueue(object : Callback<MainData> {
            override fun onFailure(call: Call<MainData>?, t: Throwable?) {
                Toast.makeText(context, "onFail!", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<MainData>?, response: Response<MainData>?) =
                if (response!!.isSuccessful) {
                    tracks = response.body().message.body.track
                    trackNameTxt.text = response.body().message.body.track.track_name
                    artistNameTxt.text = response.body().message.body.track.artist_name

                    getFav()

                } else {
                    Toast.makeText(context, "Response not successful", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    fun getLyrics() {
        val trackId = arguments?.getInt("trackId")
        val call: Call<MainData> = ApiClient.getClient.getLyrics(trackId!!)
        call.enqueue(object : Callback<MainData> {
            override fun onFailure(call: Call<MainData>?, t: Throwable?) {
                Toast.makeText(context, "onFail!", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<MainData>?, response: Response<MainData>?) =
                if (response!!.isSuccessful) {

                    lyricsTxt.text = response.body().message.body.lyrics.lyrics_body
                    copyrightTxt.text = response.body().message.body.lyrics.lyrics_copyright

                    lyricUrl = response.body().message.body.lyrics.script_tracking_url

                } else {
                    Toast.makeText(context, "Response not successful", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            moreBtn.id -> {
                val i = Intent(Intent.ACTION_VIEW, Uri.parse(lyricUrl))
                startActivity(i)
            }
//            artistNameTxt.id->{
//                //Navigation.findNavController(v).navigate(R.id.action_trackDetailFragment_to_detailArtistFragment)
//            }
            searchBtn.id -> {
                Navigation.findNavController(v)
                    .navigate(R.id.action_trackDetailFragment_to_searchFragment)
            }
            homeBtn.id -> {
                Navigation.findNavController(v)
                    .navigate(R.id.action_trackDetailFragment_to_homeScreenFragment)
            }
            toolbarFavBtn.id -> {
                Navigation.findNavController(v)
                    .navigate(R.id.action_trackDetailFragment_to_favListFragment)
            }
        }


    }
}