package com.example.ayca.musicapp.Model

import com.google.gson.annotations.SerializedName

data class ArtistList(

    @SerializedName("artist")
    val artist : Artist
)