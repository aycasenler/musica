package com.example.ayca.musicapp.Model

import com.google.gson.annotations.SerializedName

data class AlbumList(
    @SerializedName("album")
    val album : Album
)