package com.example.ayca.musicapp

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.musicapp.Model.Artist
import com.example.ayca.musicapp.Model.MainData
import com.example.ayca.musicapp.adapter.AlbumDataAdapter
import com.example.ayca.musicapp.retrofit.ApiClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailArtistFragment : Fragment(), View.OnClickListener {
    lateinit var artistNameTxt: TextView
    lateinit var recyclerView: RecyclerView
    lateinit var albumAdapter: AlbumDataAdapter
    lateinit var artistCountryTxt: TextView
    lateinit var twitterBtn: ImageButton
    lateinit var searchBtn: ImageButton
    lateinit var homeBtn: ImageButton
    lateinit var toolbarFavBtn: ImageButton
    lateinit var favBtn: ImageButton
    lateinit var url: String
    lateinit var preferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    var isClick: Boolean = false
    var favArtistList: MutableList<Artist> = mutableListOf()
    var artist: Artist? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.detail_artist_fragment, container, false)
        initView(view)

        return view
    }

    @SuppressLint("CommitPrefEdits")
    private fun initView(view: View) {
        artistNameTxt = view.findViewById(R.id.artist_name_txt)
        artistCountryTxt = view.findViewById(R.id.artist_country_txt)
        twitterBtn = view.findViewById(R.id.twitter_btn)
        favBtn = view.findViewById(R.id.fav_btn)



        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.addItemDecoration(
            DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
        )

        preferences =
            context!!.getSharedPreferences("favArtistList", Context.MODE_PRIVATE)
        editor = preferences.edit()

        searchBtn = view.findViewById(R.id.toolbar_search_btn)
        homeBtn = view.findViewById(R.id.home_screen_btn)
        toolbarFavBtn = view.findViewById(R.id.toolbar_fav_btn)

        searchBtn.setOnClickListener(this)
        homeBtn.setOnClickListener(this)
        toolbarFavBtn.setOnClickListener(this)
        twitterBtn.setOnClickListener(this)
        getAlbums()
        getArtist()


    }

    private fun getArtist() {
        val artistId = arguments?.getInt("artistId")
        val call: Call<MainData> = ApiClient.getClient.getArtist(artistId!!)
        call.enqueue(object : Callback<MainData> {
            override fun onFailure(call: Call<MainData>?, t: Throwable?) {
                Toast.makeText(context, "onFail!", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<MainData>?, response: Response<MainData>?) =
                if (response!!.isSuccessful) {
                    artistNameTxt.text =
                        response.body().message.body.artist.artist_name
                    artistCountryTxt.text =
                        response.body().message.body.artist.artist_country
                    url = response.body().message.body.artist.artist_twitter_url
                    artist = response.body().message.body.artist

                    getFav()

                } else {
                    Toast.makeText(context, "Response not successful", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    private fun getFav() {

        if (preferences.getString(
                "favArtistList",
                null
            )?.contains(artist!!.artist_name)!!
        ) {
            favBtn.setImageResource(R.drawable.fav_click_small_ic)
            isClick = true
        } else {
            favBtn.setImageResource(R.drawable.fav_small_ic)
            isClick = false
        }


        favBtn.setOnClickListener {
            if (isClick) {

                favBtn.setImageResource(R.drawable.fav_small_ic)
                isClick = false


                var artistList =
                    Gson().fromJson(
                        preferences.getString("favArtistList", ""),
                        Array<Artist>::class.java
                    ).toList()

                favArtistList.addAll(artistList)
                favArtistList.remove(artist!!)

                var favArtistListString = Gson().toJson(favArtistList)

                editor.putString("favArtistList", favArtistListString)
                editor.apply()

            } else {
                favBtn.setImageResource(R.drawable.fav_click_small_ic)
                isClick = true


                var artistList = Gson().fromJson(
                    preferences.getString("favArtistList", ""),
                    Array<Artist>::class.java
                ).toList()

                favArtistList.addAll(artistList)
                favArtistList.add(artist!!)
                var favArtistListString = Gson().toJson(favArtistList)


                editor.putString("favArtistList", favArtistListString)
                editor.apply()


            }
        }

    }

    fun getAlbums() {
        val artistId = arguments?.getInt("artistId")
        val call: Call<MainData> = ApiClient.getClient.getArtistAlbums(artistId!!)
        call.enqueue(object : Callback<MainData> {
            override fun onFailure(call: Call<MainData>?, t: Throwable?) {
                Toast.makeText(context, "onFail!", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<MainData>?, response: Response<MainData>?) =
                if (response!!.isSuccessful) {

                    albumAdapter =
                        AlbumDataAdapter(response.body().message.body.album_list, context!!)

                    recyclerView.adapter = albumAdapter
                } else {
                    Toast.makeText(context, "Response not successful", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            twitterBtn.id -> {
                val i = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(i)
            }
            searchBtn.id -> {
                Navigation.findNavController(v)
                    .navigate(R.id.action_detailArtistFragment_to_searchFragment)
            }
            homeBtn.id -> {
                Navigation.findNavController(v)
                    .navigate(R.id.action_detailArtistFragment_to_homeScreenFragment)
            }
            toolbarFavBtn.id -> {
                Navigation.findNavController(v)
                    .navigate(R.id.action_detailArtistFragment_to_favListFragment)
            }
        }

    }

}
